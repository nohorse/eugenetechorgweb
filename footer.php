<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package EugeneTech
 */

?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="container">
			<div class="site-info">
					<p>All content &copy; Eugenetech.org <?php echo date("Y") ?></p>
					<p>Built and hosted by <a href="http://mauricegriffin.com">Maurice Griffin</a> <span>&bull;</span> Designed by <a href="http://artsdigital.co">Artsdigital.co</a></p>	
				<!-- <a href="<?php //echo esc_url( __( 'http://wordpress.org/', 'eugene_tech' ) ); ?>"><?php //printf( esc_html__( 'Proudly powered by %s', 'eugene_tech' ), 'WordPress' ); ?></a>
				<span class="sep"> | </span>
				<?php //printf( esc_html__( 'Theme: %1$s by %2$s.', 'eugene_tech' ), 'eugene_tech', '<a href="http://automattic.com/" rel="designer">Automattic</a>' ); ?> -->
			</div><!-- .site-info -->
		</div>
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
