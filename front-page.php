<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package EugeneTech
 */

?>

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.5&appId=135757069793033";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<?php get_header(); ?>
	<div class="container">
		<!-- Normally I'd prefer to do all the column stuff semantically but this is a community project, why not makeit just an F12 away -->
		<div class="home-hero">
			<img src="<?php bloginfo('stylesheet_directory'); ?>/img/logo.png" alt="" class="center-block">
			<h1>eugenetech.org</h1>
			<h2 class="col-xs-12 col-s-8 col-s-offset-2 col-md-8 col-md-offset-2">Eugenetech.org produces a regular podcast highlighting Eugene, Oregon's booming tech and startup scene.</h2>
			<p class="col-xs-12 col-s-8 col-s-offset-2 col-md-6 col-md-offset-3">Currently we are teaming up with community partners to properly launch our web presence and kickoff our next podcast season. Join us below as we help build and support our thriving city.</p>
		</div>

		<div class="well subscribe col-xs-12 col-s-8 col-s-offset-2 col-md-8 col-md-offset-2">
			<p class="text-center col-md-8 col-md-offset-2">Subscribe to our email list. We'll keep you up to date with our latest episodes.</p>
			<div class="row">
				<!-- Begin MailChimp Signup Form -->
				<div id="mc_embed_signup" class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-8 col-md-offset-2">
					<form action="//EugeneTech.us12.list-manage.com/subscribe/post?u=3e1886af4796ee7e1f1a31015&amp;id=632e104099" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
					    <div id="mc_embed_signup_scroll">
						
					<div class="mc-field-group">
						<label for="mce-EMAIL" style="display:none;">Email Address </label>
						<input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL" placeholder="YOUR EMAIL">
					</div>
						<div id="mce-responses" class="clear">
							<div class="response" id="mce-error-response" style="display:none"></div>
							<div class="response" id="mce-success-response" style="display:none"></div>
						</div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
					    <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_3e1886af4796ee7e1f1a31015_632e104099" tabindex="-1" value=""></div>
					    <div class="clear"><input type="submit" class="btn btn-default btn-lg" value="SUBSCRIBE" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
					    </div>
					</form>
				</div>

<!--End mc_embed_signup-->


<!--End mc_embed_signup-->
				<!-- <form class="form-inline col-xs-12 col-md-8 col-md-offset-2 validate" action="//EugeneTech.us12.list-manage.com/subscribe/post?u=3e1886af4796ee7e1f1a31015&amp;id=632e104099" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" target="_blank" novalidate>
					<div class="form-group">
						<input class="form-control input-lg required email" type="email" placeholder="YOUR EMAIL"> -->
						<!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
				    	<!-- <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_3e1886af4796ee7e1f1a31015_632e104099" tabindex="-1" value=""></div>
					</div>
					<button type="submit" class="btn btn-default btn-lg">SUBSCRIBE</button>
				</form> -->
			</div>
			
		</div>
		
		<div class="col-xs-12 col-s-8 col-s-offset-2 col-md-8 col-md-offset-2">
			<p class="lead text-center">Hear our past episodes:</p>
			<div class="episode-links">
				<a href="https://itunes.apple.com/us/podcast/eugene-tech/id934891735?mt=2">iTunes</a>
				<a href="https://archive.org/search.php?query=creator%3A%22ELM+Robotics%22">Archive.org</a>
				<a href="http://www.stitcher.com/podcast/eugene-tech">Stitcher.com</a>
				<a href="https://feeds.feedburner.com/EugeneTech?format=xml">RSS</a>
			</div>
		</div>

		<div class="feed feed-twitter">
			<a class="twitter-timeline" href="https://twitter.com/EugTech" data-widget-id="666883231216566272">Tweets by @EugTech</a>
			<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>

		</div>
		<div class="feed feed-fb">
			<div class="fb-page" data-href="http://facebook.com/EugTech" data-width="518" data-height="600" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true" data-show-posts="true"><div class="fb-xfbml-parse-ignore"><blockquote cite="http://facebook.com/EugTech"><a href="http://facebook.com/EugTech">Eugene Tech</a></blockquote></div></div>
		</div>
	</div>
<?php get_footer(); ?>