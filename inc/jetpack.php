<?php
/**
 * Jetpack Compatibility File
 * See: https://jetpack.me/
 *
 * @package EugeneTech
 */

/**
 * Add theme support for Infinite Scroll.
 * See: https://jetpack.me/support/infinite-scroll/
 */
function eugene_tech_jetpack_setup() {
	add_theme_support( 'infinite-scroll', array(
		'container' => 'main',
		'render'    => 'eugene_tech_infinite_scroll_render',
		'footer'    => 'page',
	) );
} // end function eugene_tech_jetpack_setup
add_action( 'after_setup_theme', 'eugene_tech_jetpack_setup' );

/**
 * Custom render function for Infinite Scroll.
 */
function eugene_tech_infinite_scroll_render() {
	while ( have_posts() ) {
		the_post();
		get_template_part( 'template-parts/content', get_post_format() );
	}
} // end function eugene_tech_infinite_scroll_render
