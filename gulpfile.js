var gulp = require('gulp');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var browserSync = require('browser-sync').create();
// var livereload = require('gulp-livereload');

// gulp.task('browser-sync', function() {
    
// });

gulp.task('sass', function () {
	gulp.src('./css/**/*.scss')
		.pipe(sourcemaps.init())
		.pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
		.pipe(sourcemaps.write('./'))
		.pipe(gulp.dest('./css'))
		.pipe(browserSync.stream());
		// .pipe(livereload());
});

gulp.task('watch', function () {
	browserSync.init({
        proxy: "localhost/eugenetech"
    });
	// livereload.listen();
	gulp.watch('./css/**/*.scss', ['sass']);
	gulp.watch("./*.html").on('change', browserSync.reload);
	gulp.watch("./*.js").on('change', browserSync.reload);
});

gulp.task('default', ['watch']);